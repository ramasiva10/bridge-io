import { Fragment, useEffect, useState } from "react";
import Web3                    from "web3";
import styles                  from "./App.module.css";
import React from "react";
//import Particles from "react-tsparticles";
import { 
  ABI, 
  ETH_TOKEN_ADDRESS, 
  ETH_BRIDGE_ADDRESS, 
  ETH_BRIDGE_ABI, 
  BINANCE_END, 
  BINANCE_TOKEN_ABI, 
  BINANCE_TOKEN_ADDRESS,
  PROD_BINANCE_TOKEN_ADDRESS,
  PROD_BINANCE_END } from './contract';
import { IoArrowForwardSharp } from "react-icons/io5";
import axios from 'axios';



 // note, contract address must match the address provided by Truffle after migrations


function App() {
  const [wallet, setWallet] = useState({
    available: false,
    address: '',
    erc20_balance: 0,
    bep20_balance: 0,
    network: 0,
    error: ''
  })

  const [amount, setAmount] = useState(null)
  const [activeButton, setActiveButton] = useState(false)
  const [connectedStatus, setconnectedStatus] = useState(false)
  const [status, setStatus] = useState({
    txn_hash: '',
    message: ''
  })


  const updateBalance = () => {
    if( wallet.available){
      try{

        const web3 = new Web3(Web3.givenProvider);
        const Contract = new web3.eth.Contract(ABI, ETH_TOKEN_ADDRESS);
        Contract.methods.balanceOf(wallet.address).call({from: wallet.address})
        .then(function(result){
          let balance = Math.round( web3.utils.toBN(result) / web3.utils.toBN("1000000000000000000") )
          setWallet(oldState => ({ ...oldState, erc20_balance: balance}))
        });

        const bscWeb3 = new Web3(new Web3.providers.HttpProvider(BINANCE_END));
        const BscContract = new bscWeb3.eth.Contract(BINANCE_TOKEN_ABI, BINANCE_TOKEN_ADDRESS );

        BscContract.methods.balanceOf(wallet.address).call({from: wallet.address})
        .then(function(result){
          let balance = Math.round( web3.utils.toBN(result) / web3.utils.toBN("1000000000000000000") )
          setWallet(oldState => ({ ...oldState, bep20_balance: balance}))
        });
      } catch(error){
        setWallet({...wallet, error: error})
      }
    }
    
  }


  const connectWallet = async () => {
    if (window.ethereum) { //check if Metamask is installed
      try {
        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
        setWallet({...wallet, address: accounts[0], available: true});
            
      } catch (error) {
          return {
              connectedStatus: false,
              status: "🦊 Connect to Metamask using the button on the top right."
          }
      }
      
    } else {
      setWallet({...wallet, error: "🦊 You must install Metamask into your browser: <a class='alert-danger' href='https://metamask.io/download.html'> https://metamask.io/download.html </a>"})
      setconnectedStatus( false);
    } 
  };


  useEffect(() => {

    const web3 = new Web3(Web3.givenProvider);
    if (window.ethereum) { //check if Metamask is installed
      try {
        const chainId = window.ethereum.request({ method: 'eth_chainId' })
        window.ethereum.request({ method: 'eth_requestAccounts' }).then(function(result){
          setWallet({...wallet, address: result[0], available: true , network: chainId});
        });
        
            
      } catch (error) {
          return {
              connectedStatus: false,
              status: "🦊 Connect to Metamask using the button on the top right."
          }
      }
      
    } else {
      return {
          connectedStatus: false,
          status: "🦊 You must install Metamask into your browser: https://metamask.io/download.html"
      }
    }


    window.ethereum.on("accountsChanged", function(accounts) {
      if(accounts.length > 0){
        setWallet(oldState => ({ ...oldState, address: accounts[0], erc20_balance: 0, bep20_balance: 0, error: ''}))
      }else{
        
        window.location.reload();
        setWallet('')
      }
    });
  
    window.ethereum.on("chainChanged", function(chainId) {
      setWallet(oldState => ({ ...oldState, network: chainId, erc20_balance: 0, bep20_balance: 0, error: ''}))
    });
   
  }, []);


  useEffect(() => {
    if( wallet.available ){
    //   if (wallet.network == '0x1'){
    //     const ETH_TOKEN_ADDRESS = "";
    //     const BINANCE_TOKEN_ADDRESS = "";
    //   }else if(wallet.network == '0x3'){ 
    //     const ETH_TOKEN_ADDRESS = ETH_TOKEN_ADDRESS;
    //     const BINANCE_TOKEN_ADDRESS = BINANCE_TOKEN_ADDRESS;
        
    //   }
    try{
      const web3 = new Web3(Web3.givenProvider);
      const Contract = new web3.eth.Contract(ABI, ETH_TOKEN_ADDRESS);
      Contract.methods.balanceOf(wallet.address).call({from: wallet.address})
      .then(function(result){
        let balance = web3.utils.fromWei(result, 'ether');
        setWallet(oldState => ({ ...oldState, erc20_balance: balance}))
      }).catch(error => {
        setWallet({...wallet, error: error})
      });

      const bscWeb3 = new Web3(new Web3.providers.HttpProvider(BINANCE_END));
      const BscContract = new bscWeb3.eth.Contract(BINANCE_TOKEN_ABI, BINANCE_TOKEN_ADDRESS );

      BscContract.methods.balanceOf(wallet.address).call({from: wallet.address})
      .then(function(result){
        let balance = web3.utils.fromWei(result, 'ether')
        setWallet(oldState => ({ ...oldState, bep20_balance: balance}))
      }).catch(error => {
        setWallet({...wallet, error: error})
      });
    } catch( error ){
      setWallet({...wallet, error: error})
    }
     
      
    }
  }, [wallet.address, wallet.network, wallet.available]);


  const handleTransfer = (e) => {
    setActiveButton(true);
    e.preventDefault();
    var transactionOne, transactionTwo;
    if( wallet.available){
      const web3 = new Web3(Web3.givenProvider);
      const Contract = new web3.eth.Contract(ABI, ETH_TOKEN_ADDRESS);
      const transferAmount = web3.utils.toWei(amount.toString())
      
      Contract.methods.approve(ETH_BRIDGE_ADDRESS, transferAmount).send({from: wallet.address})
      .then(function(result){
        debugger;
        transactionOne = result.transactionHash;
        const BridgeContract = new web3.eth.Contract(ETH_BRIDGE_ABI, ETH_BRIDGE_ADDRESS);
        BridgeContract.methods.swapBoutsToBSC(transferAmount).send({from: wallet.address, value: 1000000000000000})
        .then(function(result){
          transactionTwo = result.transactionHash;
          debugger;
          axios.post('https://devbridgeapi.bouts.pro/transfer_to_bsc', {
            appr_tx: transactionOne,
            trans_tx: transactionTwo,
            amount: transferAmount.toString(),
            address: wallet.address
          })
          .then(function (response) {
            debugger;
            
            if(response.status == 200){
              updateBalance();
              setActiveButton(false)
              setAmount(0);
              setStatus(oldState => ({ ...oldState, txn_hash: response.txn_hash, message: "Transfer completed successfully !"}))
            }else{
              console.log(response)
            }
  
          })
          .catch(function (error) {
            debugger;
            setAmount(0);
            setWallet({...wallet, error: error.message})
          });

        })
        .catch(function (error) {
          debugger;
          setAmount(0);
          setWallet({...wallet, error: error.message})
        });
      }).catch(function (error) {
        debugger;
        setAmount(0);
        setWallet({...wallet, error: error.message})
      });
    }
  }

  const handleMax = () => {
    setAmount(wallet.erc20_balance)
  }

  return (
    <Fragment>
      <div className={styles.App} >
        <nav className={`navbar navbar-expand-lg mb-2 ${styles.navbar}`}>
          <div className="container">
            <a className={`navbar-brand fw-bold ${styles.link}`} href="#">BoutsPro</a>
            <button className="navbar-toggler fw-bold " type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
              <ul className="navbar-nav">
                
                <li className="nav-item p-2">
                  <a className={`nav-link btn btn-primary text-white btn-sm ${styles.connectButton}`}  aria-current="page" href="javascript:void(0);" onClick={connectWallet}>
                    { wallet.available ? "Connected" : "Connect Wallet" }
                  </a>
                </li>
                {
                  wallet.available && 
                  <li className="nav-item p-2">
                    <a className={`nav-link px-2 text-white btn-sm ${styles.walletAddressButton}`}  aria-current="page" href="javascript:void(0);" onClick={connectWallet}>
                     { `${wallet.address.slice(0,6)}...${wallet.address.slice(37,)}` }
                    </a>
                  </li>
                    
                }
              </ul>
            </div>
          </div>
        </nav>
        {
          true &&
          <div className="container">
            <div className={styles.glass}>
                <div className="row">
                  <div className="col-md-5 col-sm-12 px-2">
                    <p className="fs-2 text-start text-white fw-bold">Ethereum Mainnet</p>
                    <p className="fs-2 text-start text-white fw-bold">BOUTS ERC20 Balance</p>
                    <p className="fs-1 fw-bold text-start text-white">{ wallet.erc20_balance }</p>
                  </div>
                  <div className="col-md-2 col-sm-12 d-flex align-items-center"><IoArrowForwardSharp size={60} color="#fff"/></div>
                  <div className="col-md-5 col-sm-12 px-2">
                    <p className="fs-2 text-start text-white fw-bold">Binance Smart Chain</p>
                    <p className="fs-2 text-start text-white  fw-bold">BOUTS BEP20 Balance</p>
                    <p className="fs-1 fw-bold text-start text-white">{ wallet.bep20_balance }</p>
                  </div>
                </div>
            </div>
            <div className={styles.actions}>
              <div className="row">
              <div className="col-md-4 col-sm-12 px-2">
                  <p className="text-start fw-bold">Transfer Amount</p>
                </div>
                <div className="col-md-4 col-sm-12 px-2">
                  <p className="text-end fw-bold" onClick={handleMax} style={{ cursor: 'pointer'}}>MAX</p>
                </div>
              </div>
              <form className="form-inline" onSubmit={handleTransfer}>
                <div className="row">
                    <div className="col-md-8 col-sm-12 px-2">
                      <input className={`form-control form-control-lg fw-bold  ${styles.input}`} type="number" max={wallet.erc20_balance} min="0" required placeholder="Amount" value={amount}  onChange={(e) => { setAmount(e.target.value)} } />
                    </div>
                    <div className="col-md-4 col-sm-12 px-2">
                      <button type="sumbit" className={`btn btn-primary btn-lg w-100 ${styles.btn}`}>Transfer</button>
                    </div>
                </div>
              </form>
            </div>
            <br/>
            <div className={styles.error}>{wallet.error}</div>
          </div>
        }
        
      
      </div>
    </Fragment>
  );
}

export default App;
